package fr.cylpios.kryptage_one;
import android.support.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class KryptageUser {
    public Integer code;
    public String ip;
    public String time;
    public Boolean valide;
    public KryptageUserValidator validation;

    public KryptageUser(){
        //Default constructor for calls to Datbase
    }

    public Integer getCode() {
        return code;
    }

    public String getIp() {
        return ip;
    }

    public String getTime() {
        return time;
    }

    @NonNull
    public Boolean getValide() {
        return valide;
    }

    public KryptageUserValidator getValidation() {
        return validation;
    }
}
