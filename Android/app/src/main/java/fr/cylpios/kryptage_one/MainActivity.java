package fr.cylpios.kryptage_one;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    DatabaseReference kryptageValuesDB;
    List<KryptageUser> kuserList;
    ListView listViewValues ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        kryptageValuesDB = database.getReference();

        kuserList = new ArrayList<>();
        listViewValues = (ListView) findViewById(R.id.listViewValues);
    }

    @Override
    protected void onStart(){
        super.onStart();

        kryptageValuesDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(MainActivity.this, "Firebase Mise à jour", Toast.LENGTH_LONG).show();

                kuserList.clear();

                for (DataSnapshot valueSnapshot: dataSnapshot.getChildren()) {
                    KryptageUser kryptageUser = valueSnapshot.getValue(KryptageUser.class);
                    kuserList.add(kryptageUser);
                    Log.d("Action", "Listing User");
                }


                ArrayAdapter adapter = new KryptageUserList(MainActivity.this, kuserList);
                listViewValues.setAdapter(adapter);

                listViewValues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        KryptageUser kryptageUser = kuserList.get(position);
                        String entryId = TextUtils.join("", kryptageUser.getIp().split("\\."));
                        if(kryptageUser.valide){
                            showValidatorDialog(kryptageUser.validation.getNom(), kryptageUser.validation.getPrenom(), kryptageUser.validation.getSection(), String.valueOf(kryptageUser.getCode()));
                        }else{
                            showUpdateDialog(entryId, kryptageUser.getCode());
                        }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void showUpdateDialog(final String valuId, Integer valueCode){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_dialog, null);

        dialogBuilder.setView(dialogView);

        final EditText editTextNom = (EditText) dialogView.findViewById(R.id.editTextNom);
        final EditText editTextPrenom = (EditText) dialogView.findViewById(R.id.editTextPrenom);
        final EditText editTextSection = (EditText) dialogView.findViewById(R.id.editTextSection);

        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.update);


        dialogBuilder.setTitle("Validation du code " + String.valueOf(valueCode));

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = editTextNom.getText().toString().trim();
                String prenom = editTextPrenom.getText().toString().trim();
                String section = editTextSection.getText().toString().trim();

                if(TextUtils.isEmpty(nom)){
                    editTextNom.setError("Ne peut être vide");
                    return;
                }
                if(TextUtils.isEmpty(prenom)){
                    editTextPrenom.setError("Ne peut être vide");
                    return;
                }
                if(TextUtils.isEmpty(section)){
                    editTextSection.setError("Ne peut être vide");
                    return;
                }

                updateValue(valuId, nom, prenom, section);

                alertDialog.dismiss();
            }
        });
    }

    private void showValidatorDialog(String nom, String prenom, String section, String code){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.validator_dialog, null);

        final TextView textViewValidator = (TextView) dialogView.findViewById(R.id.textViewValidator);
        final TextView textViewValidatorCode = (TextView) dialogView.findViewById(R.id.textViewValidatorCode);
        final ImageButton buttonClose = (ImageButton) dialogView.findViewById(R.id.imageButtonClose);

        textViewValidatorCode.setText(code);
        textViewValidator.setText(nom + " -- " + prenom + " -- " + section);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private boolean updateValue(String ip, String nom, String prenom, String section){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(ip).child("validation");
        KryptageUserValidator kryptageValidator = new KryptageUserValidator(nom, prenom, section);

        databaseReference.setValue(kryptageValidator);

        FirebaseDatabase.getInstance().getReference().child(ip).child("valide").setValue(true);
        Toast.makeText(MainActivity.this, "Mise à jours", Toast.LENGTH_LONG).show() ;

        return true;
    }
}
